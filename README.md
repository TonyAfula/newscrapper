Newscrapper is a PHP web application best for crawlings news from popular Nigerian websites.




REQUIREMENTS
------------

The minimum requirement for this project is your Web server supports PHP >5.4.0+.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install dependencies using this command:

~~~
composer install
~~~

Now you should be able to access the application through the following APIs

~~~
Pulls news articles from Linda IkeJi's Blog

http://localhost/newscrapper/web/news/lindaikejis
~~~

~~~
Show news details from Linda IkeJi's Blog

POST http://localhost/newscrapper/web/news/linda-ikeji-detail
param link https://www.lindaikejisblog.com/2019/11/exchange-rates-cbn-governor-godwin-emefiele-to-appear-before-senate-committee-today-2.html

~~~


~~~
Pulls news articles from Laila News

http://localhost/newscrapper/web/news/lailanews
~~~

~~~
Show news details from Laila News

POST http://localhost/newscrapper/web/news/laila-news-detail
param link https://lailasnews.com/1-4-million-nigerians-jostle-for-5000-vacancies-in-nscdc/

~~~

~~~
Pulls news articles from Vanguard

http://localhost/newscrapper/web/news/vanguard
~~~

~~~
Show news details from Vanguard

POST http://localhost/newscrapper/web/news/vanguard-detail
param link https://www.vanguardngr.com/2019/11/the-necessity-of-nigerias-igis-project/

~~~

~~~
Pulls news articles from Daily Trust

http://localhost/newscrapper/web/news/dailytrust
~~~




~~~
Show news details from Daily Trust

POST http://localhost/newscrapper/web/news/daily-trust-detail
param link https://www.dailytrust.com.ng/zamfara-assembly-collects-data-on-citizens-needs.html

~~~