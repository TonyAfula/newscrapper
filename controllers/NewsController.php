<?php

namespace app\controllers;


use Yii;
use app\models\News;
use Goutte\Client;
use yii\rest\ActiveController;

class NewsController extends ActiveController {

    public $modelClass = 'app\models\News';
    public $newsArticles;
    public $category = "";
    public $count = 0;
    public $content ="";

    /**
     * Crawl Linda Ikejis Blog
     */
    public function actionLindaikejis() {
        $this->newsArticles = [];

        $lindaIkejiUrl = "https://www.lindaikejisblog.com/";
        $client = new Client();
        $crawler = $client->request('GET', $lindaIkejiUrl);
        $crawler->filter('article.story_block')->each(function ($node) {
            $newArticle = new News();
            $newArticle->title = $node->filter('.story_title')->text();
            //$newArticle->author= "Linda Ikeji";
            $newArticle->description = $node->filter('.story_description')->text();
            $newArticle->photo = $node->filter('img')->attr('src');
            //$newArticle->published= $node->filter('.post_age')->text();
            $newArticle->link = $node->filter('a.pull-right')->attr('href');


            array_push($this->newsArticles, $newArticle);
        });

        return $this->newsArticles;
    }
    
    public function actionLindaIkejiDetail() {
        $this->newsArticles = [];
        
        $request = Yii::$app->request;
        $link = $request->post('link');
        
        if(empty($link)){
            return ["success"=>true,"message"=>"Link cannot be empty"];
        }
        
        
        $client = new Client();
        $crawler = $client->request('GET', $link);
        
        $title= $crawler->filter('title')->text();
        $imageSrc  = $crawler->filter('article.big_story img')->first()->attr('src');
        $crawler->filter('article.big_story p')->each(function ($node) {
            
            //var_dump($node);
            $text = $node->text();
            //if(!empty($text)){
                $this->content .= "<p>{$text}</p>"; 
            //}
           
            
        });


        
        return ["title"=>$title,"image"=>$imageSrc,"content"=> $this->content];
    }

    /**
     * Crawl Laila News website
     */
    public function actionLailanews() {
        $this->newsArticles = [];

        $lailaNewsUrl = "https://lailasnews.com/";
        $client = new Client();
        $crawler = $client->request('GET', $lailaNewsUrl);
        $crawler->filter('article')->each(function ($node) {
            $newArticle = new News();
            $newArticle->title = $node->filter('h3.entry-title')->text();
            $newArticle->description = $node->filter('.entry-summary')->text();
            $newArticle->photo = $node->filter('img.wp-post-image')->attr('src');
            $newArticle->link = $node->filter('a.g1-link-more')->attr('href');

            array_push($this->newsArticles, $newArticle);
        });

        return $this->newsArticles;
    }
    
    public function actionLailaNewsDetail() {
        $this->newsArticles = [];
        
        $request = Yii::$app->request;
        $link = $request->post('link');
        
        if(empty($link)){
            return ["success"=>true,"message"=>"Link cannot be empty"];
        }
        
        
        $client = new Client();
        $crawler = $client->request('GET', $link);
        
        $title= $crawler->filter('title')->text();
        $imageSrc =$crawler->filter('div.entry-content img')->first()->attr('src');
        $crawler->filter('div.entry-content p')->each(function ($node) {
            
            //var_dump($node);
            $text = $node->text();
            //if(!empty($text)){
                $this->content .= "<p>{$text}</p>"; 
            //}
           
            
        });


        
        return ["title"=>$title,"image"=>$imageSrc,"content"=> $this->content];
    }

    /**
     * Crawl Vanguard website
     */
    public function actionVanguard() {
        $this->newsArticles = [];

        $vanguardUrl = "https://www.vanguardngr.com/";
        $client = new Client();
        $crawler = $client->request('GET', $vanguardUrl);



        
        $crawler->filter('ul.vanguard-slides')->children('li.rtp-slide')->each(function ($node) {
            $newArticle = new News();
            $newArticle->title = $node->filter('h2.rtp-slide-title')->first()->text();
            $newArticle->description = $node->filter('div.rtp-slide-content')->children("p")->first()->text();
            $newArticle->category = "Top Stories";

            $newArticle->photo = $node->filter('img.wp-post-image')->attr('src');
            $newArticle->link = $node->filter('a.rtp-thumb')->attr('href');

            $this->count += 1;


//            $nextSliderLink =$crawler->filter("div#rt-home-slider")->children("div.rtp-next");
//            $nextSlide = $client->click($nextSliderLink);

            array_push($this->newsArticles, $newArticle);
        });



        /*
          $crawler->filter('aside#vanguard_home_categories-10')->each(function ($node) {
          $this->category = $node->filter('h2.widget-title')->first()->text();

          print_r($this->category);
          $node->filter("ul")->each(function($listNode) {
          $newArticle = new News();
          $newArticle->category = $this->category;

          $listItem = $listNode->filter('li');
          $newArticle->title = $listItem->children("h2.rtp-cat-post-title")->first()->text();
          $newArticle->photo = $listItem->children('img.wp-post-image')->first()->attr('src');
          $newArticle->link = $listNode->filter('a.rtp-thumb')->attr('href');

          array_push($this->newsArticles, $newArticle);
          });
          }); */
        return [$this->newsArticles];
    }

    /**
     * Crawl Daily Trust website
     */
    public function actionDailytrust() {
        $this->newsArticles = [];

        $dailyTrustUrl = "https://www.dailytrust.com.ng/";
        $client = new Client();
        $crawler = $client->request('GET', $dailyTrustUrl);
        $crawler->filter('article.carousel-entry')->each(function ($node) {
            $newArticle = new News();
            $newArticle->title = $node->filter('h1.news_title')->text();
            $newArticle->description = $node->filter('div.entry-excerpt')->text();
            //$newArticle->category = "";

            $newArticle->photo = $node->filter('img')->attr('src');
            $newArticle->link = $node->filter('h1.news_title')->children("a")->first()->attr('href');


            array_push($this->newsArticles, $newArticle);
        });

        return $this->newsArticles;
    }

    public function actionDailyTrustDetail() {
        $this->newsArticles = [];
        
        $request = Yii::$app->request;
        $link = $request->post('link');
        
        if(empty($link)){
            return ["success"=>true,"message"=>"Link cannot be empty"];
        }
        
        
        $client = new Client();
        $crawler = $client->request('GET', $link);
        
        $title= $crawler->filter('title')->text();
        $imageSrc =$crawler->filter('article img.wp-post-image')->first()->attr('src');
        $crawler->filter('article p')->each(function ($node) {
            
            //var_dump($node);
            $text = $node->text();
            //if(!empty($text)){
                $this->content .= "<p>{$text}</p>"; 
            //}
           
            
        });


        
        return ["title"=>$title,"image"=>$imageSrc,"content"=> $this->content];
    }
    
    
    /**
     * Crawl Vanguard website
     */
    public function actionVanguardDetail() {
        $this->newsArticles = [];
        
        $request = Yii::$app->request;
        $link = $request->post('link');
        
        if(empty($link)){
            return ["success"=>true,"message"=>"Link cannot be empty"];
        }
        
        
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $title= $crawler->filter('title')->text();

        $entryContent =$crawler->filter('div.entry-content');
        //$imageSrc = "";
        $imageSrc =$crawler->filter('div.entry-content img')->first()->attr('src');
        $entryContent->children('p')->each(function ($node) {
            
            //var_dump($node);
            $text = $node->text();
            //if(!empty($text)){
                $this->content .= "<p>{$text}</p>"; 
            //}
           
            
        });


        
        return ["title"=>$title,"image"=>$imageSrc,"content"=> $this->content];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;
        return $behaviors;
    }

}
